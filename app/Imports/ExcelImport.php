<?php

namespace App\Imports;

use App\Models\Book1;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Illuminate\Support\Facades\DB;
use Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExcelImport implements ToCollection, WithHeadingRow, WithCalculatedFormulas
{
    public $tableName;

    public function __construct($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $rows = array_filter($rows->toArray());
        if (empty($rows)) {
            return false;
        }

        // Data type detection and processing
        $queryString = $this->dataTypeDetection($rows);
        // Table create
        if (DB::statement($queryString)) {
            $modelName = ucfirst($this->tableName);
            //or you can pass the model name if you wanna create tables like company1,company2
            //Artisan::call('make:model', ['name' => $modelName]);
            // Data insert
            DB::table($this->tableName . 's')->insert($rows);
        } else {
            return false;
        }
    }

    public function dataTypeDetection($data)
    {
        $columnNames = array_keys($data[0]);
        $dataType = null;
        $count = 0;
        $tableName = $this->tableName . 's';
        $columnWiseDataType = "CREATE TABLE $tableName (
            id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        ";

        foreach ($data[1] as $key => $item) {
            if (gettype($item) == 'string' && (strlen($item) > 255)) {
                $dataType = 'TEXT';
            } elseif (gettype($item) == 'string' && (strlen($item) < 255)) {
                $dataType = 'VARCHAR(255)';
            } elseif (gettype($item) == 'integer') {
                $dataType = 'INT(11)';
            } elseif (gettype($item) == 'double') {
                $dataType = 'DOUBLE';
            } elseif (gettype($item) == 'boolean') {
                $dataType = 'BOOLEAN';
            } elseif (Json::decode($item)) {
                $dataType = 'JSON';
            }

            if ($dataType) {
                $columnWiseDataType .= $key . ' ' . $dataType;
            }
            if (count($data[1]) != ($count + 1)) {
                $columnWiseDataType .= ',';
            }
            $count++;
        }
        return $columnWiseDataType . ')';
    }

    function extractDateTimeFormat($string)
    {
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $string)) {
            return true;
        } elseif (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/', $string)) {
            return true;
        } elseif (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}$/', $string)) {
            return true;
        } elseif (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $string)) {
            return true;
        } elseif (preg_match('/^\d{1}\/\d{2}\/\d{4}$/', $string)) {
            return true;
        } elseif (preg_match('/^\d{2}\.\d{2}\.\d{4}$/', $string)) {
            return true;
        } else {
            return false;
        }
    }
}
