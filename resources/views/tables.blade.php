@extends('layouts.app')

@section('content')
    <div class="panel panel-default" style="margin-top: 20px">
        <div class="panel-heading">
            <h5>Import Excel Data</h5>
        </div>
        <div class="panel-body">
            <!-- Display Validation Errors -->
            {{--@include('common.errors')--}}
            <!-- New Table Form -->
            <form action="excel/store"  enctype="multipart/form-data" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Table Name -->

                <div class="form-group">
                    <label for="task" class="col-sm-3 control-label">File</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file" class="form-control">
                    </div>
                </div>

                <!-- Add Table Button -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-plus"></i> Import
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Current Tables -->
    @if (count($tables) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Tables
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">
                    <!-- Table Headings -->
                    <thead>
                    <th>Table</th>
                    <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                    @foreach ($tables as $table)
                        <tr>
                            <!-- Table Name -->
                            <td class="table-text">
                                <div>{{ $table->Tables_in_excel_process }}</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection
